CS++ FPGA README
================
This package contains all actors using FPGA solutions for CS++. Right now we have the *CSPP_FPGA-PPG* actor which creates a TTL pulsed pattern useful for the control
of measurement sequences, and the *CSPP_FPA-MCS*, a Multi-channel scaler. Both actors need additional drivers and an FPGA card from National Instruments (7811/3R, 7841R, 7856R).
They have both inherited from base actors within the *CSPP_DeviceBase* package where also a default GUI can be found.

| FPGA class name | type | device driver | additional Ini-file entries |
| ----------------------------- | ---- | ------------- | --------------------------- |
| CSPP_FPGA-MCS                 | driver | [NI-FPGA-MCS](https://git.gsi.de/EE-LV/Drivers/NI-FPGA-MCS) | none |
| CSPP_FPGA-PPG                 | driver | [NI-FPGA-PPG](https://git.gsi.de/EE-LV/Drivers/NI-FPGA-PPG) | none |

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.
2. [**CSPP_DeviceBase**](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): This package contains the base class for both actors as well as their default GUI.

FPGA-MCS
--------
A more detailed description of this type of device can be found in the description of the base class. For this FPGA solution just note that you download and install also the
instrument driver. The source code is identical for the different FPGA cards from National Instruments (right now: 7811/3R, 7856R; for others please ask me). However
once the device driver is downloaded one may have to change the used Bitfile (compiled code to be executed by the FPGA card). This code may differ for each different card and the user
has to configure within the driver which FPGA card should be used. For this open the *initialize.vi* in the driver and choose another Bitfile in the *Open FPGA VI reference*:

![First configuration of the FPGA-MCS driver](Docs/mcs_choosebitfile.png)

FPGA-PPG
--------
Like with the FPGA-MCS the device driver has to be configured for the usage of another FPGA card (default is 7841R). For this open the *FPGA-PPG_resource_handler.vi* and
choose another Bitfile in the *Open FPGA VI reference*:

![First configuration of the FPGA-PPG driver](Docs/ppg_choosebitfile.png)

The driver also includes a document with further information, like the In/Output configuration and possible error codes.

Related information
---------------------------------
Author: D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.